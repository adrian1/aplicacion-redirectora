# Aplicación Redirectora

# URL Redirection Server

This Python script implements a simple HTTP server that generates HTTP responses with random redirection status codes (3xx) and redirects users to randomly chosen URLs from a predefined list.

## How it Works

1. The server listens for incoming HTTP connections on a specified port.

2. When a client connects, it generates a random redirection status code (e.g., 301, 302) and selects a random URL from a predefined list.

3. The server constructs an HTTP response with the chosen status code, a corresponding message, and an HTML body containing a hyperlink to the randomly chosen URL.

4. The response is sent back to the client, redirecting them to the selected URL.

## Usage

1. Clone the repository or download the `redirect_server.py` script.

2. Ensure you have Python 3 installed on your system.

3. Run the script using the command:

   ```bash
   python3 redirect_server.py
   ```
   
4. Access the server from a web browser or using HTTP client tools like cURL. The server will respond with a redirection response containing a randomly chosen URL from the predefined list.

## Customization

You can customize the behavior of the server by modifying the following variables in the script:

- `myPort`: Specify the port number on which the server should listen.
- `redirections`: Dictionary containing HTTP status codes (3xx) and their corresponding messages.
- `redirection_urls`: List of URLs to which users can be redirected. You can add or remove URLs as needed.

## Dependencies

This script relies on the `socket` and `random` modules in Python, which are part of the standard library and do not require additional installation.

